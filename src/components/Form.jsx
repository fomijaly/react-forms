import React, { useEffect } from "react";
import { useState } from "react";

function Form() {
  const [value, setValue] = useState(0);
  const [lastName, setLastname] = useState("");
  const [firstName, setFirstname] = useState("");
  const [age, setAge] = useState(0);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  function validate(input) {
    return input > 18;
  }

  useEffect(() => {
    if (!validate(age)) {
      setErrorMessage("Vous devez être majeur");
    } else {
      setErrorMessage("");
    }
  }, [value]);

  function handleSubmit(event) {
    event.preventDefault();
    setLastname(lastName);
    setFirstname(firstName);
    setAge(age);
    setEmail(email);
    setPassword(password);
    console.log(
      `Bonjour ${firstName} ${lastName}, vous avez ${age} ans. Vous avez indiqué "${email}" en tant qu'adresse mail et votre mot de passe est : ${password}`
    );
  }
  return (
    <>
      <form onSubmit={handleSubmit} onChange={setValue}>
        <label htmlFor="lastName">Nom</label>
        <input
          type="text"
          name="lastName"
          value={lastName}
          onChange={(e) => setLastname(e.target.value)}
        />

        <label htmlFor="firstName">Prénom</label>
        <input
          type="text"
          name="firstName"
          value={firstName}
          onChange={(e) => setFirstname(e.target.value)}
        />

        <label htmlFor="age">Age</label>
        <input
          type="number"
          name="age"
          value={age}
          onChange={(e) => {
            setAge(e.target.value);
            setValue(e.target.value);
          }}
        />
        {errorMessage && <div>{errorMessage}</div>}

        <label htmlFor="mail">Adresse e-mail</label>
        <input
          type="text"
          name="mail"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />

        <label htmlFor="password">Mot de passe</label>
        <input
          type="text"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type="submit">Soumettre</button>
      </form>
    </>
  );
}

export default Form;
